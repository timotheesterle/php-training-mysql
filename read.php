<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Randonnées</title>
    <link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <h1>Liste des randonnées</h1>
    <table>

    <?php

    include('dbconnect.php');

    $sql_randonnees = $database->query('SELECT * FROM hiking');
    $randonnees = $sql_randonnees->fetchAll();

    ?>

    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Difficulté</th>
                <th>Distance (km)</th>
                <th>Durée</th>
                <th>Dénivelé (m)</th>
                <th>Disponibilité</th>
                <th>Supression</th>
            </tr>
        </thead>

        <tbody>

            <?php foreach ($randonnees as $randonnee): ?>

            <tr>
                <td><?= $randonnee['id'] ?></td>
                <td>
                    <a href="update.php?id=<?= $randonnee['id'] ?>">
                        <?= $randonnee['name'] ?>
                    </a>
                </td>
                <td><?= $randonnee['difficulty'] ?></td>
                <td><?= $randonnee['distance'] ?></td>
                <td><?= $randonnee['duration'] ?></td>
                <td><?= $randonnee['height_difference'] ?></td>
                <td><?= $randonnee['available'] ? 'Disponible' : 'Non disponible' ?></td>
                <td><a href="delete.php?id=<?= $randonnee['id'] ?>">Supprimer</a></td>
            </tr>

            <?php endforeach; ?>

        </tbody>

    </table>

    <br>
    <a href="create.php">Ajouter une randonnée</a>

  </body>
</html>
