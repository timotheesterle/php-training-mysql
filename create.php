<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body>

	<?php if ($_POST):

	include('dbconnect.php');

	$sql = "INSERT INTO hiking (
		name,
		difficulty,
		distance,
		duration,
		height_difference,
		available
	) VALUES (
		?,
		?,
		?,
		?,
		?,
		?
	)";


	$insert = $database->prepare($sql);
	$insert->execute(
		[
			$_POST['name'],
			$_POST['difficulty'],
			intval($_POST['distance']),
			$_POST['duration'],
			intval($_POST['height_difference']),
			intval($_POST['available'])
		]
	);

	?>

	<mark>Données ajoutées</mark><br>
	<?php endif; ?>

	<a href="/read.php">Liste des données</a>
	<h1>Ajouter</h1>
	<form action="" method="post">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="number" name="distance" value="">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="time" name="duration" value="" step="2">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="number" name="height_difference" value="">
		</div>
		<div>
			<label for="available">Disponibilité</label>
			<select name="available">
				<option value="1">Disponible</option>
				<option value="0">Non disponible</option>
			</select>
		</div>
		<button type="submit" name="button">Envoyer</button>
	</form>
</body>
</html>
