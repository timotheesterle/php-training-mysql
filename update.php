<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body>

	<?php

	if ($_POST):

	include('dbconnect.php');

	$sql = "UPDATE hiking
			SET
				name = ?,
				difficulty = ?,
				distance = ?,
				duration = ?,
				height_difference = ?,
				available = ?
			WHERE id= ? ";


	$update = $database->prepare($sql);
	$update->execute(
		[
			$_POST['name'],
			$_POST['difficulty'],
			intval($_POST['distance']),
			$_POST['duration'],
			intval($_POST['height_difference']),
			intval($_POST['available']),
			$_GET['id']
		]
	);

	?>

	<mark>Données modifiées</mark><br>
	<?php endif; ?>

	<?php

	if ($_GET):

	include('dbconnect.php');

	$sql_randonnees = $database->query("SELECT * FROM hiking WHERE id={$_GET['id']}");
	$randonnees = $sql_randonnees->fetchAll();

	?>

	<a href="read.php">Liste des données</a>
	<h1>Modifier</h1>
	<form action="" method="post">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="<?= $randonnees[0]['name']?>">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option
					value="très facile"
					<?= $randonnees[0]['difficulty'] === 'très facile' ? 'selected' : '' ?>
				>
					Très facile
				</option>
				<option
					value="facile"
					<?= $randonnees[0]['difficulty'] === 'facile' ? 'selected' : '' ?>
				>
					Facile
				</option>
				<option
					value="moyen"
					<?= $randonnees[0]['difficulty'] === 'moyen' ? 'selected' : '' ?>
				>
					Moyen
				</option>
				<option
					value="difficile"
					<?= $randonnees[0]['difficulty'] === 'difficile' ? 'selected' : '' ?>
				>
					Difficile
				</option>
				<option
					value="très difficile"
					<?= $randonnees[0]['difficulty'] === 'très difficile' ? 'selected' : '' ?>
				>
					Très difficile
				</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="number" name="distance" value="<?= $randonnees[0]['distance']?>">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="time" name="duration" value="<?= $randonnees[0]['duration']?>">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="number" name="height_difference" value="<?= $randonnees[0]['height_difference']?>">
		</div>
		<div>
			<label for="available">Disponibilité</label>
			<select name="available">
				<option value="1" <?= $randonnees[0]['available'] ? 'selected' : '' ?>>Disponible</option>
				<option value="0" <?= !$randonnees[0]['available'] ? 'selected' : '' ?>>Non disponible</option>
			</select>
		</div>
		<button type="submit" name="button">Envoyer</button>
	</form>

	<?php else: ?>

	<mark>Erreur</mark>

	<?php endif; ?>
</body>
</html>
